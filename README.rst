Construction et maintenance de logiciels
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ce dépôt contient des fragments de code présentés dans le cadre du cours
INF3135 Construction et maintenance de logiciels à l'automne 2018.

License
=======

Tout le contenu de ce projet est sous `license GPLv3
<https://www.gnu.org/licenses/gpl-3.0.en.html>`__.
