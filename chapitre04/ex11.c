#include <stdio.h>

int main() {
	char *file_name = "ex11.c";
	FILE *fd;
	int res;
	char c;

	fd = fopen(file_name, "r");

	if(fd != NULL) {
		printf("Fichier ouvert avec succès.\n");
	} else {
		printf("Erreur lors de l'ouverture du fichier.\n");
		return 1;
	}

	while ((c = fgetc(fd)) != EOF) {
		putchar(c);
	}

	res = fclose(fd);
	if(res < 0) {
		printf("Erreur lors de la fermeture du fichier.\n");
		return 1;
	}

	return 0;
}
